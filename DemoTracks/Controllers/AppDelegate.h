//
//  AppDelegate.h
//  A
//
//  Created by Patrik Svensson on 07/04/15.
//  Copyright (c) 2015 Patrik Svensson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

