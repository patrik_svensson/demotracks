//
//  DetailCellTableViewCell.h
//  DemoTracks
//
//  Created by Patrik Svensson on 23/08/15.
//  Copyright (c) 2015 Patrik Svensson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackDetailPropertyCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *keyLabel;
@property (nonatomic, weak) IBOutlet UILabel *valueLabel;
@end
