//
//  DetailCellTableViewCell.m
//  DemoTracks
//
//  Created by Patrik Svensson on 23/08/15.
//  Copyright (c) 2015 Patrik Svensson. All rights reserved.
//

#import "TrackDetailPropertyCell.h"

@implementation TrackDetailPropertyCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
