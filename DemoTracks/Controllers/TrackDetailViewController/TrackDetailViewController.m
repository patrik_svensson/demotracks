//
//  DetailViewController.m
//  A
//
//  Created by Patrik Svensson on 07/04/15.
//  Copyright (c) 2015 Patrik Svensson. All rights reserved.
//

#import "TrackDetailViewController.h"
#import "TrackDetailPropertyCell.h"

@interface TrackDetailViewController ()
@property (nonatomic) NSTimer *timer;
@end

@implementation TrackDetailViewController {
}
static NSArray *contentRows;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
    //[self.tableView registerClass:[DetailCell class]forCellReuseIdentifier:@"DetailCell"];
    contentRows = @[@"title", @"band", @"genre", @"plays", @"rating", @"downloads"];
}

- (void)configureView {
    // Update the user interface for the detail item.
//    if (self.detailItem) {
//        self.title = [self.detailItem title];
//    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    

    TrackDetailPropertyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailCell" forIndexPath:indexPath];
    cell.keyLabel.text = NSLocalizedString(contentRows[indexPath.row], nil);
    cell.valueLabel.text = [NSString stringWithFormat:@"%@", [self.detailItem valueForKey:contentRows[indexPath.row]]];
   
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}


#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
            
        // Update the view.
        [self configureView];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
