//
//  DetailViewController.h
//  A
//
//  Created by Patrik Svensson on 07/04/15.
//  Copyright (c) 2015 Patrik Svensson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Track.h"
#import "TrackDetailPropertyCell.h"

@interface TrackDetailViewController : UITableViewController

@property (strong, nonatomic) Track *detailItem;

@end

