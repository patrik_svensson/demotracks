//
// Created by Patrik Svensson on 13/04/15.
// Copyright (c) 2015 Patrik Svensson. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@class TrackListViewController;
@class Track;
@class UISearchBar;

@interface TrackSearchController : NSObject
- (id)initWithSearchBar:(UISearchBar *)bar contentsController:(TrackListViewController *)controller
    didSelectTrackBlock:(void (^)(Track *))block;

@property (strong, nonatomic, readonly) UISearchController *searchController;

-(void)searchDisplayControllerDidBeginSearch;

@end