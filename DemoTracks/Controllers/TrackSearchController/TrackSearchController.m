//
// Created by Patrik Svensson on 13/04/15.
// Copyright (c) 2015 Patrik Svensson. All rights reserved.
//

#import "TrackSearchController.h"
#import "TrackListViewController.h"
#import "Track.h"


@implementation TrackSearchController


- (id)initWithSearchBar:(UISearchBar *)bar contentsController:(TrackListViewController *)controller didSelectTrackBlock:(void (^)(Track *))block {
    self = [super init];


    if (self) {
        //_searchController = [[UISearchController alloc] initWithSearchResultsController:<#(UIViewController *)searchResultsController#>];
    }


    return self;
}

-(void)searchDisplayControllerDidBeginSearch {
    self.searchController.searchBar.showsCancelButton = false;
}

@end