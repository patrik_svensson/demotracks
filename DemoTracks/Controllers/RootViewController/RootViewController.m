//
//  RootViewController.m
//  A
//
//  Created by Patrik Svensson on 13/08/15.
//  Copyright (c) 2015 Patrik Svensson. All rights reserved.
//

@import AVFoundation;
@import MediaPlayer;

#import "RootViewController.h"
#import "RootNavigationController.h"
#import "TrackListViewController.h"
#import "MusicService.h"
#import "MBProgressHUD.h"
#import "DDLogger.h"

@interface RootViewController ()
@property (weak, nonatomic) IBOutlet UILabel *message;
@property (weak, nonatomic) IBOutlet UIButton *playPause;
@property (strong, nonatomic) IBOutlet UIView *background;
@end

static void *DemoTracksStatusObservationContext = &DemoTracksStatusObservationContext;

@implementation RootViewController {
    UIImage *play, *pause;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    play = [UIImage imageNamed:@"play"];
    pause = [UIImage imageNamed:@"pause"];
    self.message.text = @"";
    self.playPause.hidden = YES;
    [self remoteCommandStatus];
    [self remoteCommandsEnable:NO];
    [self remoteCommandStatus];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [self becomeFirstResponder];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
}

- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    DDLogVerbose(@"remoteControlReceivedWithEvent");
    [self remoteCommandStatus];
    
    UIEventSubtype type = event.subtype;
    if (type == UIEventSubtypeRemoteControlPause) {
        [self pausePlaying];
    }    else if (type == UIEventSubtypeRemoteControlPlay) {
        [self resumePlaying];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)playPauseAction:(id)sender {
    if ([MusicService sharedClient].isPlaying) {
        [self pausePlaying];
    } else {
        [self resumePlaying];
    }
}

- (void)resumePlaying {
    [[MusicService sharedClient] play];
    [self showPauseButton];
}

- (void)pausePlaying {
    [[MusicService sharedClient] pause];
    [self showPlayButton];
}

- (void)showPauseButton {
    [self.playPause setImage:pause forState:UIControlStateNormal];
}

- (void)showPlayButton {
    [self.playPause setImage:play forState:UIControlStateNormal];
}

- (void)playTrack:(Track *)track {
    DDLogVerbose(@"%s playTrack: %@ : %@", __PRETTY_FUNCTION__, track.title, track.band);
    self.message.text = [NSString stringWithFormat:@"%@\n%@", track.title, track.band];
    
    Track *trackToPlay = [track copy];
    NSURL *mURL = [trackToPlay audioURL];
    self.playPause.hidden = NO;
    
    [self showHUD];
    [MusicService sharedClient].delegate = self;
    [[MusicService sharedClient] playURL:mURL success:^{
        [self hideHUD];
        DDLogVerbose(@"%s success", __PRETTY_FUNCTION__);
        [self showPauseButton];
    } failure:^(NSError *error) {
        [self hideHUD];
        DDLogError(@"%s<->failure Error: %@", __PRETTY_FUNCTION__, error);
        // Show alert
        [self showErrorAlert:error];
        self.message.text = @"";
        self.playPause.hidden = YES;
    }];
}

// delegate method
- (void)playingEnded:(NSError *)error {
    if (error) {
        DDLogError(@"%s - %@", __PRETTY_FUNCTION__, error);
    }
    self.message.text = @"";
    self.playPause.hidden = YES;
}

- (void)synchUserInterface {
    if ([[MusicService sharedClient] isPlaying]) {
        [self showPauseButton];
    } else {
        [self showPlayButton];
    }
}

- (void)showHUD {
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    });
}

- (void)hideHUD {
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}

- (void)showErrorAlert:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[error localizedDescription]
                                                            message:[error localizedFailureReason]
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK",nil)
                                                  otherButtonTitles:nil];
        [alertView show];
    });
}

- (void)timerFired:(NSTimer *)timer {
    NSString *currentTimeInSec = [NSString stringWithFormat:@"%f", [[MusicService sharedClient] currentTime]];
    DDLogVerbose(@"Current time: %@", currentTimeInSec);
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    RootNavigationController *rootNavigationController = segue.destinationViewController;
    rootNavigationController.trackController = self;
    TrackListViewController *masterViewController = (TrackListViewController*)rootNavigationController.topViewController;
    masterViewController.trackController = self;
}

#pragma mark - audio setup

- (void)audioSetup {
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];

    NSError *setCategoryError = nil;
    BOOL success = [audioSession setCategory:AVAudioSessionCategoryPlayback error:&setCategoryError];
    if (!success) {
        /* handle the error condition */
        DDLogError(@"Error starting audio session");
    }

    NSError *activationError = nil;
    success = [audioSession setActive:YES error:&activationError];
    if (!success) {
        /* handle the error condition */
        DDLogError(@"Error starting audio session");
    }
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)remoteCommandsEnable:(BOOL)enable {
    MPRemoteCommandCenter *commandCenter = [MPRemoteCommandCenter sharedCommandCenter];
    commandCenter.previousTrackCommand.enabled = enable;
    commandCenter.nextTrackCommand.enabled = enable;
    commandCenter.skipBackwardCommand.enabled = enable;
    commandCenter.skipForwardCommand.enabled = enable;
    commandCenter.playCommand.enabled = enable;
    commandCenter.pauseCommand.enabled = enable;
}

- (void)remoteCommandStatus {
    MPRemoteCommandCenter *commandCenter = [MPRemoteCommandCenter sharedCommandCenter];
    DDLogVerbose(@"previousTrackCommand.isEnabled %d", commandCenter.previousTrackCommand.isEnabled);
    DDLogVerbose(@"nextTrackCommand.isEnabled %d", commandCenter.nextTrackCommand.isEnabled);
    DDLogVerbose(@"skipBackwardCommand.isEnabled %d", commandCenter.skipBackwardCommand.isEnabled);
    DDLogVerbose(@"skipForwardCommand.isEnabled %d", commandCenter.skipForwardCommand.isEnabled);
    DDLogVerbose(@"playCommand.isEnabled %d", commandCenter.playCommand.isEnabled);
    DDLogVerbose(@"pauseForwardCommand.isEnabled %d", commandCenter.pauseCommand.isEnabled);
}

@end
