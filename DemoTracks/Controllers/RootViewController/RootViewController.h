//
//  RootViewController.h
//  A
//
//  Created by Patrik Svensson on 13/08/15.
//  Copyright (c) 2015 Patrik Svensson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrackControllerDelegate.h"
#import "MusicService.h"

@interface RootViewController : UIViewController <TrackControllerDelegate, MusicServicedelegate>

@end
