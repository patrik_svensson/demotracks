//
//  MasterViewController.h
//  A
//
//  Created by Patrik Svensson on 07/04/15.
//  Copyright (c) 2015 Patrik Svensson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrackControllerDelegate.h"

extern NSString *DT_SHOWDETAIL_IDENTITY;

@interface TrackListViewController : UITableViewController
@property (nonatomic) id<TrackControllerDelegate> trackController;
@end

