//
//  MasterViewController.m
//  A
//
//  Created by Patrik Svensson on 07/04/15.
//  Copyright (c) 2015 Patrik Svensson. All rights reserved.
//


#import "TrackListViewController.h"
#import "TrackDetailViewController.h"
#import <AFNetworking.h>
#import <Mantle.h>
#import "Track.h"
#import "DataService.h"
#import "UIView+Toast.h"
#import "MBProgressHUD.h"
#import "DDLogger.h"

@interface TrackListViewController () <UISearchResultsUpdating, UISearchBarDelegate>

@property NSMutableArray *objects;
@property(nonatomic, strong) UISearchController *trackSearchController;

@end


#pragma Constants
NSString *DT_SHOWDETAIL_IDENTITY = @"ShowDetail";


@implementation TrackListViewController {
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTrackSearchController];
}

- (void)setupTrackSearchController {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.trackSearchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.trackSearchController.searchResultsUpdater = self;
    self.trackSearchController.dimsBackgroundDuringPresentation = NO;
    self.trackSearchController.hidesNavigationBarDuringPresentation = NO;
    self.trackSearchController.searchBar.frame = CGRectMake(self.trackSearchController.searchBar.frame.origin.x, self.trackSearchController.searchBar.frame.origin.y, self.trackSearchController.searchBar.frame.size.width, 44.0);

    self.trackSearchController.searchBar.placeholder = NSLocalizedString(@"Search Demolatar.se", @"Search field text");
    self.trackSearchController.searchBar.delegate = self;
    
    self.tableView.tableHeaderView = self.trackSearchController.searchBar;

    self.definesPresentationContext = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)insertNewObject:(id)sender {
    if (!self.objects) {
        self.objects = [[NSMutableArray alloc] init];
    }
    [self.objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:DT_SHOWDETAIL_IDENTITY]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        Track *selectedTrack = self.objects[indexPath.row];
        [[segue destinationViewController] setDetailItem:selectedTrack];
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    Track *track = self.objects[indexPath.row];
    cell.textLabel.text = [track title];
    cell.detailTextLabel.text = [track band];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self playTrack:self.objects[indexPath.row]];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Search

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    // -updateSearchResultsForSearchController: is called when the controller is being dismissed to allow those who are using the controller they are search as the results controller a chance to reset their state. No need to update anything if we're being dismissed.
        DDLogVerbose(@"Search controller is %@active.", searchController.active?@"":@"not");
        DDLogVerbose(@"Search bar text:%@", searchController.searchBar.text);
    //[searchController.searchBar setShowsCancelButton:NO animated:NO];

}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
// called when keyboard search button pressed
    DDLogVerbose(@"Search clicked");
    DDLogVerbose(@"Search bar text:%@", searchBar.text);
    [self showHUD];
    [[DataService sharedClient] search:searchBar.text
                       completionBlock:^(NSArray *results, NSError *error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self hideHUD];
                [self.view makeToast:error.localizedDescription
                           duration:4.0
                           position:CSToastPositionCenter];
            });
        } else {
            [self hideHUD];
            if  ([results count] == 0) {
                [self.view makeToast:NSLocalizedString(@"Sorry, no tracks were found", @"No result")
                            duration:4.0
                            position:CSToastPositionCenter];
            } else {
                self.objects = [results mutableCopy];
                //DDLogVerbose(@"Results %@", results);
                [self.tableView reloadData];
            }
        }
    }];
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)showErrorAlert:(NSError *)error {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[error localizedDescription]
                                                        message:[error localizedFailureReason]
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK",nil)
                                              otherButtonTitles:nil];
    [alertView show];
}

- (void)showHUD {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)hideHUD {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - Play Track

- (void) playTrack:(Track *)track {
    if (self.trackController && [self.trackController respondsToSelector:@selector(playTrack:)]) {
        [self.trackController playTrack:track];
    }
}


@end
