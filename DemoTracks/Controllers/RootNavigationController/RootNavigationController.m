//
//  RootNavigationControllerViewController.m
//  DemoTracks
//
//  Created by Patrik Svensson on 18/08/15.
//  Copyright (c) 2015 Patrik Svensson. All rights reserved.
//

#import "RootNavigationController.h"
#import "TrackListViewController.h"

@interface RootNavigationController ()

@end

@implementation RootNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    TrackListViewController *masterViewController = segue.destinationViewController;
    masterViewController.trackController = self.trackController;
}

@end
