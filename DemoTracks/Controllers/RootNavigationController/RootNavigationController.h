//
//  RootNavigationControllerViewController.h
//  DemoTracks
//
//  Created by Patrik Svensson on 18/08/15.
//  Copyright (c) 2015 Patrik Svensson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrackControllerDelegate.h"

@interface RootNavigationController : UINavigationController
@property (nonatomic) NSString *test;
@property (nonatomic)id<TrackControllerDelegate> trackController;
@end
