//
//  MusicService.m
//  DemoTracks
//
//  Created by Patrik Svensson on 15/09/15.
//  Copyright © 2015 Patrik Svensson. All rights reserved.
//

@import AVFoundation;
@import MediaPlayer;
@import CoreMotion;

#import "MusicService.h"
#import "DDLogger.h"

@interface MusicService () {
    NSTimer *timeout;
}

@property (copy) void(^success)(void);
@property (copy) void(^failure)(NSError *);

@property (nonatomic) AVPlayer *player;
@property (nonatomic) AVPlayerItem *playerItem;
@end



@implementation MusicService

static void *playbackBufferEmpty = &playbackBufferEmpty;
static void *status = &status;

+ (instancetype)sharedClient {
    static MusicService *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[MusicService alloc] init];
        [_sharedClient audioSetup];
    });
    return _sharedClient;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _player = [[AVPlayer alloc] init];
    }
    return self;
}

- (void)reset {
     _player = [[AVPlayer alloc] init];
}

- (void)playURL:(NSURL *)URL success:(void (^)(void))success failure:(void (^)(NSError *))failure {
    self.success = success;
    self.failure = failure;

    // If we already have a playerItem, then remove the old observations
    if (self.playerItem) {
        [self.playerItem removeObserver:self forKeyPath:@"status"];
        [self.playerItem removeObserver:self forKeyPath:@"playbackBufferEmpty"];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:AVPlayerItemDidPlayToEndTimeNotification
                                                      object:self.playerItem];
    }
    
    // Create new Asset and playerItem
    AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:URL options:nil];
    self.playerItem = [AVPlayerItem playerItemWithAsset:avAsset];
    
    // Start observing the newly created playerItem
    [self.playerItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew context:status];
    [self.playerItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew context:playbackBufferEmpty];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:self.playerItem];
    
    // Set the newly creaeted playerItem as the item to play
    [self.player replaceCurrentItemWithPlayerItem:self.playerItem];
    
    // Start playing
    [self.player play];
    
    //timeout = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(networkTimeout:) userInfo:nil repeats:NO];

}

-(void)playerItemDidReachEnd:(NSNotification *)notification {
    DDLogVerbose(@"%s  ", __PRETTY_FUNCTION__);
    if ([self.delegate respondsToSelector:@selector(playingEnded:)]) {
        [self.delegate playingEnded:nil];
    }
}

#if 1
- (void)networkTimeout:(NSTimer *) timer {
    [timeout invalidate];
    
    [self.player pause];
    NSDictionary *userInfo = @{
                               NSLocalizedDescriptionKey: NSLocalizedString(@"NetworkTimeout.", nil),
                               NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"The operation timed out.", nil),
                               NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Disconnected", nil)
                               };
    NSError *error = [NSError errorWithDomain:@"Network"
                                         code:NSNetServicesTimeoutError
                                     userInfo:userInfo];
    self.failure(error);
}
#endif

- (BOOL)isPlaying {
    return self.player && self.player.rate > 0.0;
}

- (void)pause {
    if (self.player) {
        [self.player pause];
    }
}

- (void)play {
    if (self.player) {
        [self.player play];
    }
}

- (double)currentTime {
    double returnValue;
    if (self.player) {
        returnValue = self.player.currentTime.value / self.player.currentTime.timescale;
    }
    return returnValue;
}

#if 0
- (void) playerItemFailedToPlayToEndTime:(NSNotification *)notification
{
    NSError *error = notification.userInfo[AVPlayerItemFailedToPlayToEndTimeErrorKey];
    DDLogError(@"Error: %@", error);
}
#endif

- (void)playerItemDidPlayToEndTime:(NSNotification *)notification {
    DDLogVerbose(@"Did play to end of time");
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    DDLogVerbose(@"Change: %@", change);
    
    if (context == status) {
        
        /* AVPlayerItem "status" property value observer. */
        if (object == self.playerItem && [keyPath isEqualToString:@"status"]) {
            AVPlayerItemStatus status = self.playerItem.status;
            switch (status) {
                case AVPlayerItemStatusReadyToPlay: {
                    DDLogVerbose(@"Success");
                    self.success();
                    //[timeout invalidate];
                    break;
                }
                case AVPlayerItemStatusFailed: {
                    DDLogError(@"Failed");
                    // Creating a new player after a failure
                    [self reset];
                    //[timeout invalidate];
                    self.failure(self.playerItem.error);
                    break;
                }
                case AVPlayerItemStatusUnknown: {
                    DDLogVerbose(@"StatusUnknown");
                    break;
                }
            }
        }
    } else if (context == playbackBufferEmpty) {
        DDLogVerbose(@"playbackBufferEmpty");
        //[self networkTimeout:nil];
    }
}

- (void)audioSetup {
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    
    NSError *setCategoryError = nil;
    BOOL success = [audioSession setCategory:AVAudioSessionCategoryPlayback error:&setCategoryError];
    if (!success) {
        /* handle the error condition */
        DDLogError(@"Error starting audio session");
    }
    
    NSError *activationError = nil;
    success = [audioSession setActive:YES error:&activationError];
    if (!success) {
        /* handle the error condition */
        DDLogError(@"Error starting audio session");
    }
}


@end
