
#import <Foundation/Foundation.h>

@interface DataService : NSObject

+(instancetype)sharedClient;

- (void)search:(NSString *)artistNameOrTrackTitle completionBlock:(void(^)(NSArray *results, NSError *error))block;

@end
