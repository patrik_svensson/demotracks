#import <AFNetworking.h>
#import <Mantle.h>
#import "Track.h"
#import "DataService.h"
#import "DTConfiguration.h"
#import "DDLogger.h"

@implementation DataService {
    AFHTTPRequestOperationManager *manager;
}

+ (instancetype)sharedClient {
    static DataService *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[DataService alloc] initWithBaseURL:[NSURL URLWithString:DTConfigurationAPIEndPoint]];
    });
    return _sharedClient;
}

- (instancetype)initWithBaseURL:(NSURL *)baseURL {
    if (self = [super init]) {
        if (baseURL) {
            manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];
            manager.responseSerializer = [AFJSONResponseSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        } else {
            return nil;
        }
    }
    return self;
}

- (void)search:(NSString *)artistNameOrTrackTitle completionBlock:(void(^)(NSArray *results, NSError *error))block {
    [manager GET:@"search.php" parameters:@{@"q":artistNameOrTrackTitle}
     success:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSError *error;
         DDLogVerbose(@"%@", [operation.response allHeaderFields]);
         NSArray *trackArray = [MTLJSONAdapter modelsOfClass:[Track class] fromJSONArray:responseObject error:&error];
         if (error) {
             // Json parsing failed;
             block(nil,error);
             DDLogError(@"Error: %@", error);
         }
         block(trackArray, error);
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         block(nil,error);
         DDLogError(@"Error: %@", error);
     }];
}

@end
