//
//  MusicService.h
//  DemoTracks
//
//  Created by Patrik Svensson on 15/09/15.
//  Copyright © 2015 Patrik Svensson. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MusicServicedelegate <NSObject>

- (void)playingEnded:(NSError *)error;

@end


@interface MusicService : NSObject

@property (nonatomic)id<MusicServicedelegate> delegate;

+ (instancetype)sharedClient;

- (void)playURL:(NSURL *)URL success:(void (^)(void))success failure:(void (^)(NSError *))failure;

- (BOOL)isPlaying;
- (void)pause;
- (void)play;

- (double)currentTime;

@end
