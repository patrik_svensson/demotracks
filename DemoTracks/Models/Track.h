#import <Foundation/Foundation.h>
#import <Mantle.h>

@interface Track : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSNumber *identifier;
@property (nonatomic, copy, readonly) NSDate *creationDate;
@property (nonatomic, copy, readonly) NSString *band;
@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) NSURL *audioURL;
@property (nonatomic, copy, readonly) NSString *genre;
@property (nonatomic, copy, readonly) NSNumber *plays;
@property (nonatomic, copy, readonly) NSNumber *rating;
@property (nonatomic, copy, readonly) NSNumber *downloads;
@property (nonatomic, copy, readonly) NSURL *webURL;

@end

