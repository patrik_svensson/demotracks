#import "Track.h"

@implementation Track

+ (NSDateFormatter *)dateFormatter {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    return dateFormatter;
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"identifier": @"id",
             @"creationDate": @"date",
             @"band": @"band",
             @"title": @"track",
             @"audioURL": @"tracklink",
             @"genre": @"genre",
             @"plays": @"plays",
             @"rating": @"rating",
             @"downloads": @"downloads",
             @"webURL": @"web",
             };
}

+ (NSValueTransformer *)creationDateJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^(NSString *str) {
        return [self.dateFormatter dateFromString:str];
    } reverseBlock:^(NSDate *date) {
        return [self.dateFormatter stringFromDate:date];
    }];
}

+ (NSValueTransformer *)urlWithSpaceJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^(NSString *str) {
        //NSURL
        NSString *encodedUrlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        return [NSURL URLWithString:encodedUrlString];
    } reverseBlock:^(NSURL *url) {
        // NSString
        NSError* error = nil;
        NSStringEncoding encoding;
        NSString *str = [NSString stringWithContentsOfURL:url usedEncoding:&encoding error:&error];
        if (!error) {
            return str;
        } else {
            return @"";
        }
    }];
}

+ (NSValueTransformer *)audioURLJSONTransformer {
    return [self urlWithSpaceJSONTransformer];
}

+ (NSValueTransformer *)webURLJSONTransformer {
    return [self urlWithSpaceJSONTransformer];
}

@end
