//
//  TrackControllerDelegate.h
//  DemoTracks
//
//  Created by Patrik Svensson on 19/08/15.
//  Copyright (c) 2015 Patrik Svensson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Track.h"

@protocol TrackControllerDelegate <NSObject>

- (void)playTrack:(Track *)track;

@end
