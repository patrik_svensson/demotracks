//
// Created by Patrik Svensson on 17/09/15.
// Copyright (c) 2015 Patrik Svensson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CocoaLumberjack.h>

#ifdef DEBUG
static const DDLogLevel ddLogLevel = DDLogLevelVerbose;
#else
static const DDLogLevel ddLogLevel = DDLogLevelWarn;
#endif

@interface DDLogger : NSObject

@end