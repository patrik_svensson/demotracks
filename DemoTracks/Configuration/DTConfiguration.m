//
//  DTConfiguration.m
//  DemoTracks
//
//  Created by Patrik Svensson on 17/09/15.
//  Copyright © 2015 Patrik Svensson. All rights reserved.
//

#import "DTConfiguration.h"

@implementation DTConfiguration

NSString *const DTConfigurationAPIEndPoint = @"http://www.demolatar.se/api";
NSString *const DTConfigurationInfoPage = @"http://www.demolatar.se";

@end
