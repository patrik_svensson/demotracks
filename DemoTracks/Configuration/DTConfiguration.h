//
//  DTConfiguration.h
//  DemoTracks
//
//  Created by Patrik Svensson on 17/09/15.
//  Copyright © 2015 Patrik Svensson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DTConfiguration : NSObject

extern NSString *const DTConfigurationAPIEndPoint;
extern NSString *const DTConfigurationInfoPage;


@end
