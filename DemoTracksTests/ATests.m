//
//  ATests.m
//  ATests
//
//  Created by Patrik Svensson on 07/04/15.
//  Copyright (c) 2015 Patrik Svensson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <Mantle.h>
#import "Track.h"

@interface ATests : XCTestCase

@end

@implementation ATests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

#if 0
- (void)testWXCondition {
    NSError *error = nil;
    NSString *jsonString = @"{\"name\":\"adam\"}";
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
    WXCondition *track = [MTLJSONAdapter modelOfClass:[WXCondition class] fromJSONDictionary:jsonDictionary error:&error];
    
   // XCTAssert([track.plays isEqualToNumber:@73], @"Pass");
    
}
#endif

- (void)testTrack {
    NSError *error;

    NSString *jsonString = @"{\"id\":\"11078\",\"date\":\"2013-04-25 00:40:18\",\"band\":\"pelle ekblad\",\"track\":\"bankiren fran djursholm\",\"tracklink\":\"http://www.demolatar.se/demo/pelle ekblad - bankiren fran djursholm.mp3\",\"genre\":\"Pop\",\"plays\":\"73\",\"rating\":\"0\",\"downloads\":\"3\",\"web\":\"\"} ";
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
    
    Track *track = [MTLJSONAdapter modelOfClass:[Track class] fromJSONDictionary:jsonDictionary error:&error];
    
    XCTAssert([track.plays isEqualToNumber:@73], @"Pass");
    XCTAssertFalse(track.audioURL == nil);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
