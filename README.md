# README #

To get this iOS app up and running you need to install CocoaPods and run the pod install command. After that you open the workspace file in Xcode.

This app streams music from [Demolatar](http://www.demolatar.se)
and uses the api that can be seen [here](http://www.demolatar.se/api/).

You can see a short video of the user interface on my [Tumblr page](http://patriksvensson.tumblr.com/).